package pl.edu.pwsztar.service.impl;

import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.ChessMoveService;

import java.util.Arrays;
import java.util.List;

@Service
public class ChessMoveServiceImpl implements ChessMoveService {

    @Override
    public Boolean bishopFigureMove(FigureMoveDto figureMoveDto) {
        int startRow;
        int startColumn;
        int endRow;
        int endColumn;

        List<String> startPosition = Arrays.asList(figureMoveDto.getStart().split("_"));
        List<String> endPosition = Arrays.asList(figureMoveDto.getDestination().split("_"));

        startRow = Integer.parseInt(startPosition.get(1));
        endRow = Integer.parseInt(endPosition.get(1));

        startColumn = convertToNumber(startPosition.get(0));
        endColumn = convertToNumber(endPosition.get(0));

        return (Math.abs(startRow - endRow) == Math.abs(startColumn - endColumn));
    }

    private int convertToNumber(String key) {
        char znak=key.charAt(0);
        int kod_ascii;
        kod_ascii = (int)znak;
        return kod_ascii-96;
    }
}
