package pl.edu.pwsztar.service;

import pl.edu.pwsztar.domain.dto.FigureMoveDto;

public interface ChessMoveService {
    Boolean bishopFigureMove(FigureMoveDto figureMoveDto);
}
